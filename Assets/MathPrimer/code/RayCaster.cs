﻿// © 2018 adrian.licensing@gmail.com This Software is made available under the MIT License.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MathPrimer
{
    public class RayCaster : MonoBehaviour
    {
        public float amplitudeX = 1f;
        public float amplitudeY = 1f;

        public float oscillationPeriodFactor = 1f;

        public bool enableShrinking;
        public float laserEnergy = 1f;
        

        public List<SphereCollider> sphereList = new List<SphereCollider>();
       

        public

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (enableShrinking)
            {
                Ray ray = CalcRay();

                RaycastHit hitInfo;
                //bool isHit = Physics.Raycast(ray, out hitInfo);
                bool isHit = RaycastTargetSpheres(ray, out hitInfo, sphereList);

                if (isHit)
                {
                    //TODO: shrink spheres with custom raycast
                    Shrink(hitInfo);
                }
            }
        }


        private void Shrink(RaycastHit hitInfo)
        {
            Transform hitTransform = hitInfo.collider.transform;
            hitTransform.localScale -= 0.1f * Time.deltaTime * Vector3.one * laserEnergy;
            hitTransform.localScale = Vector3.Max(hitTransform.localScale, Vector3.zero); //not letting it get negative
        }



        private bool RaycastTargetSpheres(Ray ray, out RaycastHit info, List<SphereCollider> sphereList)
        {
            info = new RaycastHit();
            
            float minDist = -1f;
            RaycastHit hitAtMinDist = new RaycastHit();
            bool foundIntersection = false;
            int countIntersections = 0;
            foreach (SphereCollider sphere in sphereList)
            {
                // assuming global scale is uniform
                info = new RaycastHit(); //hope it's needed
                bool isHit = AnalyticalSphereRaycast(sphere, ray, out info);
                //bool isHit = GeometricalSphereRaycast(sphere, ray, out info);
                if (isHit)
                {
                    if (!foundIntersection) foundIntersection = true;                    
                    if(info.distance < minDist || minDist < 0f)
                    {
                        minDist = info.distance;
                        hitAtMinDist = info;
                        countIntersections++;
                    }
                }

            }
            if (foundIntersection) info = hitAtMinDist;
            Debug.Log("#Intersections: " + countIntersections);
            return foundIntersection;          
        }

        bool GeometricalSphereRaycast(SphereCollider sphere, Ray ray, out RaycastHit info)
        {
            
            // TODO: check if it will work w/o the lossyscale
            Vector3 center = sphere.transform.TransformPoint(sphere.center);
            float radius = sphere.radius * sphere.transform.lossyScale.x;

            Vector3 closest = ray.origin + ray.direction * Vector3.Dot(ray.direction, center - ray.origin);
            float distanceToLine = Vector3.Distance(closest, center);

            if (distanceToLine > radius)
            {
                // no hit
                info = new RaycastHit();
                return false;
            }
            else
            {
                float a = Mathf.Sqrt(radius * radius - distanceToLine * distanceToLine);
                Vector3 entry = closest - a * ray.direction;

                info = new RaycastHit
                {
                    point = entry,
                    normal = (entry - center).normalized,
                    distance = distanceToLine
                };
                return true;
            }
        }

        private bool AnalyticalSphereRaycast(SphereCollider sphere, Ray ray, out RaycastHit info)
        {
            info = new RaycastHit();

            Vector3 center = sphere.transform.TransformPoint(sphere.center);
            float radius = sphere.radius * sphere.transform.lossyScale.x;

            Vector3 distSphereRayOrigin = ray.origin - center;
            
            float b = 2 * Vector3.Dot(ray.direction, distSphereRayOrigin),
                  c = distSphereRayOrigin.sqrMagnitude - radius * radius,
                  discr = b * b - 4 * c;

            if (discr < 0) return false;
            else
            {
                float sqrtDiscr = Mathf.Sqrt(discr);
                float t0 = (-b - sqrtDiscr) / 2;
                float t1 = (-b + sqrtDiscr) / 2;

                if (t0 > 0)
                {
                    info.point = ray.origin + t0 * ray.direction;

                }
                else
                {
                    info.point = ray.origin + t1 * ray.direction;
                }
                info.normal = (info.point - center).normalized;
                info.distance = Vector3.Distance(info.point, ray.origin);
                return true;
            }
        }

        //TODO
        private bool RaycastPlane()
        {

            // 

            return false;
        }

        private void OnDrawGizmos()
        {
            Ray ray = CalcRay();

            RaycastHit hitInfo;
            //bool isHit = Physics.Raycast(ray, out hitInfo); 
            bool isHit = RaycastTargetSpheres(ray, out hitInfo, sphereList);            

            if (isHit)
            {                
                
                DrawRaycastSphereHitGizmo(ray, hitInfo);
            }
            else
            {
                DrawRaycastGizmo(ray);
            }
        }

        private static void DrawRaycastGizmo(Ray ray)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(ray.origin, ray.origin + 100f * ray.direction);
            Debug.Log("No hit");
        }

        private static void DrawRaycastSphereHitGizmo(Ray ray, RaycastHit hitInfo)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(ray.origin, hitInfo.point);
            Gizmos.DrawWireSphere(hitInfo.point, 0.1f);
            Gizmos.DrawLine(hitInfo.point, hitInfo.point + hitInfo.normal);
            //Debug.Log("Hit " + hitInfo.collider.gameObject.name + " at " + hitInfo.distance);
            Debug.Log("Hit sphere at " + hitInfo.distance);
        }

        private Ray CalcRay()
        {
            float angleY = Mathf.Sin(Time.time * 2f * Mathf.PI / 1f / oscillationPeriodFactor) * amplitudeY;
            float angleX = Mathf.Sin(Time.time * 2f * Mathf.PI / 7f / oscillationPeriodFactor) * amplitudeX;

            Quaternion rayRotation = Quaternion.Euler(angleX, angleY, 0f);

            Vector3 rayDirectionLocal = rayRotation * Vector3.forward; // rotates Vector3.forward by rayRotation

            Vector3 rayDirectionInWorld = transform.TransformDirection(rayDirectionLocal);  // same as transofrm.rotation * rayDirectionLocal 

            Vector3 origin = transform.position;
            Vector3 direction = rayDirectionInWorld;

            Ray ray = new Ray(origin, direction);
            return ray;
        }
    }
}
